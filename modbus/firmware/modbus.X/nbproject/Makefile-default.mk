#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/modbus.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/modbus.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/modbus/functions/mbfunccoils.c ../src/modbus/functions/mbfuncdiag.c ../src/modbus/functions/mbfuncdisc.c ../src/modbus/functions/mbfuncholding.c ../src/modbus/functions/mbfuncinput.c ../src/modbus/functions/mbfuncother.c ../src/modbus/functions/mbutils.c ../src/modbus/rtu/mbcrc.c ../src/modbus/rtu/mbrtu.c ../src/port/portevent.c ../src/port/portserial.c ../src/port/porttimer.c ../src/demo.c ../src/modbus/mb.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/172270788/mbfunccoils.o ${OBJECTDIR}/_ext/172270788/mbfuncdiag.o ${OBJECTDIR}/_ext/172270788/mbfuncdisc.o ${OBJECTDIR}/_ext/172270788/mbfuncholding.o ${OBJECTDIR}/_ext/172270788/mbfuncinput.o ${OBJECTDIR}/_ext/172270788/mbfuncother.o ${OBJECTDIR}/_ext/172270788/mbutils.o ${OBJECTDIR}/_ext/1909107292/mbcrc.o ${OBJECTDIR}/_ext/1909107292/mbrtu.o ${OBJECTDIR}/_ext/1019015877/portevent.o ${OBJECTDIR}/_ext/1019015877/portserial.o ${OBJECTDIR}/_ext/1019015877/porttimer.o ${OBJECTDIR}/_ext/1360937237/demo.o ${OBJECTDIR}/_ext/108032392/mb.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/172270788/mbfunccoils.o.d ${OBJECTDIR}/_ext/172270788/mbfuncdiag.o.d ${OBJECTDIR}/_ext/172270788/mbfuncdisc.o.d ${OBJECTDIR}/_ext/172270788/mbfuncholding.o.d ${OBJECTDIR}/_ext/172270788/mbfuncinput.o.d ${OBJECTDIR}/_ext/172270788/mbfuncother.o.d ${OBJECTDIR}/_ext/172270788/mbutils.o.d ${OBJECTDIR}/_ext/1909107292/mbcrc.o.d ${OBJECTDIR}/_ext/1909107292/mbrtu.o.d ${OBJECTDIR}/_ext/1019015877/portevent.o.d ${OBJECTDIR}/_ext/1019015877/portserial.o.d ${OBJECTDIR}/_ext/1019015877/porttimer.o.d ${OBJECTDIR}/_ext/1360937237/demo.o.d ${OBJECTDIR}/_ext/108032392/mb.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/172270788/mbfunccoils.o ${OBJECTDIR}/_ext/172270788/mbfuncdiag.o ${OBJECTDIR}/_ext/172270788/mbfuncdisc.o ${OBJECTDIR}/_ext/172270788/mbfuncholding.o ${OBJECTDIR}/_ext/172270788/mbfuncinput.o ${OBJECTDIR}/_ext/172270788/mbfuncother.o ${OBJECTDIR}/_ext/172270788/mbutils.o ${OBJECTDIR}/_ext/1909107292/mbcrc.o ${OBJECTDIR}/_ext/1909107292/mbrtu.o ${OBJECTDIR}/_ext/1019015877/portevent.o ${OBJECTDIR}/_ext/1019015877/portserial.o ${OBJECTDIR}/_ext/1019015877/porttimer.o ${OBJECTDIR}/_ext/1360937237/demo.o ${OBJECTDIR}/_ext/108032392/mb.o

# Source Files
SOURCEFILES=../src/modbus/functions/mbfunccoils.c ../src/modbus/functions/mbfuncdiag.c ../src/modbus/functions/mbfuncdisc.c ../src/modbus/functions/mbfuncholding.c ../src/modbus/functions/mbfuncinput.c ../src/modbus/functions/mbfuncother.c ../src/modbus/functions/mbutils.c ../src/modbus/rtu/mbcrc.c ../src/modbus/rtu/mbrtu.c ../src/port/portevent.c ../src/port/portserial.c ../src/port/porttimer.c ../src/demo.c ../src/modbus/mb.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/modbus.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX795F512L
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/172270788/mbfunccoils.o: ../src/modbus/functions/mbfunccoils.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/172270788" 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfunccoils.o.d 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfunccoils.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/172270788/mbfunccoils.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/172270788/mbfunccoils.o.d" -o ${OBJECTDIR}/_ext/172270788/mbfunccoils.o ../src/modbus/functions/mbfunccoils.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/172270788/mbfuncdiag.o: ../src/modbus/functions/mbfuncdiag.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/172270788" 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncdiag.o.d 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncdiag.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/172270788/mbfuncdiag.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/172270788/mbfuncdiag.o.d" -o ${OBJECTDIR}/_ext/172270788/mbfuncdiag.o ../src/modbus/functions/mbfuncdiag.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/172270788/mbfuncdisc.o: ../src/modbus/functions/mbfuncdisc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/172270788" 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncdisc.o.d 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncdisc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/172270788/mbfuncdisc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/172270788/mbfuncdisc.o.d" -o ${OBJECTDIR}/_ext/172270788/mbfuncdisc.o ../src/modbus/functions/mbfuncdisc.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/172270788/mbfuncholding.o: ../src/modbus/functions/mbfuncholding.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/172270788" 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncholding.o.d 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncholding.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/172270788/mbfuncholding.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/172270788/mbfuncholding.o.d" -o ${OBJECTDIR}/_ext/172270788/mbfuncholding.o ../src/modbus/functions/mbfuncholding.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/172270788/mbfuncinput.o: ../src/modbus/functions/mbfuncinput.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/172270788" 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncinput.o.d 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncinput.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/172270788/mbfuncinput.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/172270788/mbfuncinput.o.d" -o ${OBJECTDIR}/_ext/172270788/mbfuncinput.o ../src/modbus/functions/mbfuncinput.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/172270788/mbfuncother.o: ../src/modbus/functions/mbfuncother.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/172270788" 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncother.o.d 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncother.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/172270788/mbfuncother.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/172270788/mbfuncother.o.d" -o ${OBJECTDIR}/_ext/172270788/mbfuncother.o ../src/modbus/functions/mbfuncother.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/172270788/mbutils.o: ../src/modbus/functions/mbutils.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/172270788" 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbutils.o.d 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbutils.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/172270788/mbutils.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/172270788/mbutils.o.d" -o ${OBJECTDIR}/_ext/172270788/mbutils.o ../src/modbus/functions/mbutils.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1909107292/mbcrc.o: ../src/modbus/rtu/mbcrc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1909107292" 
	@${RM} ${OBJECTDIR}/_ext/1909107292/mbcrc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1909107292/mbcrc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1909107292/mbcrc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/1909107292/mbcrc.o.d" -o ${OBJECTDIR}/_ext/1909107292/mbcrc.o ../src/modbus/rtu/mbcrc.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1909107292/mbrtu.o: ../src/modbus/rtu/mbrtu.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1909107292" 
	@${RM} ${OBJECTDIR}/_ext/1909107292/mbrtu.o.d 
	@${RM} ${OBJECTDIR}/_ext/1909107292/mbrtu.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1909107292/mbrtu.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/1909107292/mbrtu.o.d" -o ${OBJECTDIR}/_ext/1909107292/mbrtu.o ../src/modbus/rtu/mbrtu.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1019015877/portevent.o: ../src/port/portevent.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1019015877" 
	@${RM} ${OBJECTDIR}/_ext/1019015877/portevent.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019015877/portevent.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1019015877/portevent.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/1019015877/portevent.o.d" -o ${OBJECTDIR}/_ext/1019015877/portevent.o ../src/port/portevent.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1019015877/portserial.o: ../src/port/portserial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1019015877" 
	@${RM} ${OBJECTDIR}/_ext/1019015877/portserial.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019015877/portserial.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1019015877/portserial.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/1019015877/portserial.o.d" -o ${OBJECTDIR}/_ext/1019015877/portserial.o ../src/port/portserial.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1019015877/porttimer.o: ../src/port/porttimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1019015877" 
	@${RM} ${OBJECTDIR}/_ext/1019015877/porttimer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019015877/porttimer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1019015877/porttimer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/1019015877/porttimer.o.d" -o ${OBJECTDIR}/_ext/1019015877/porttimer.o ../src/port/porttimer.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/demo.o: ../src/demo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/demo.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/demo.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/demo.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/1360937237/demo.o.d" -o ${OBJECTDIR}/_ext/1360937237/demo.o ../src/demo.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/108032392/mb.o: ../src/modbus/mb.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/108032392" 
	@${RM} ${OBJECTDIR}/_ext/108032392/mb.o.d 
	@${RM} ${OBJECTDIR}/_ext/108032392/mb.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/108032392/mb.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/108032392/mb.o.d" -o ${OBJECTDIR}/_ext/108032392/mb.o ../src/modbus/mb.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
else
${OBJECTDIR}/_ext/172270788/mbfunccoils.o: ../src/modbus/functions/mbfunccoils.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/172270788" 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfunccoils.o.d 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfunccoils.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/172270788/mbfunccoils.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/172270788/mbfunccoils.o.d" -o ${OBJECTDIR}/_ext/172270788/mbfunccoils.o ../src/modbus/functions/mbfunccoils.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/172270788/mbfuncdiag.o: ../src/modbus/functions/mbfuncdiag.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/172270788" 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncdiag.o.d 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncdiag.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/172270788/mbfuncdiag.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/172270788/mbfuncdiag.o.d" -o ${OBJECTDIR}/_ext/172270788/mbfuncdiag.o ../src/modbus/functions/mbfuncdiag.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/172270788/mbfuncdisc.o: ../src/modbus/functions/mbfuncdisc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/172270788" 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncdisc.o.d 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncdisc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/172270788/mbfuncdisc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/172270788/mbfuncdisc.o.d" -o ${OBJECTDIR}/_ext/172270788/mbfuncdisc.o ../src/modbus/functions/mbfuncdisc.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/172270788/mbfuncholding.o: ../src/modbus/functions/mbfuncholding.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/172270788" 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncholding.o.d 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncholding.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/172270788/mbfuncholding.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/172270788/mbfuncholding.o.d" -o ${OBJECTDIR}/_ext/172270788/mbfuncholding.o ../src/modbus/functions/mbfuncholding.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/172270788/mbfuncinput.o: ../src/modbus/functions/mbfuncinput.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/172270788" 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncinput.o.d 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncinput.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/172270788/mbfuncinput.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/172270788/mbfuncinput.o.d" -o ${OBJECTDIR}/_ext/172270788/mbfuncinput.o ../src/modbus/functions/mbfuncinput.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/172270788/mbfuncother.o: ../src/modbus/functions/mbfuncother.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/172270788" 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncother.o.d 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbfuncother.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/172270788/mbfuncother.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/172270788/mbfuncother.o.d" -o ${OBJECTDIR}/_ext/172270788/mbfuncother.o ../src/modbus/functions/mbfuncother.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/172270788/mbutils.o: ../src/modbus/functions/mbutils.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/172270788" 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbutils.o.d 
	@${RM} ${OBJECTDIR}/_ext/172270788/mbutils.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/172270788/mbutils.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/172270788/mbutils.o.d" -o ${OBJECTDIR}/_ext/172270788/mbutils.o ../src/modbus/functions/mbutils.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1909107292/mbcrc.o: ../src/modbus/rtu/mbcrc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1909107292" 
	@${RM} ${OBJECTDIR}/_ext/1909107292/mbcrc.o.d 
	@${RM} ${OBJECTDIR}/_ext/1909107292/mbcrc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1909107292/mbcrc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/1909107292/mbcrc.o.d" -o ${OBJECTDIR}/_ext/1909107292/mbcrc.o ../src/modbus/rtu/mbcrc.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1909107292/mbrtu.o: ../src/modbus/rtu/mbrtu.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1909107292" 
	@${RM} ${OBJECTDIR}/_ext/1909107292/mbrtu.o.d 
	@${RM} ${OBJECTDIR}/_ext/1909107292/mbrtu.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1909107292/mbrtu.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/1909107292/mbrtu.o.d" -o ${OBJECTDIR}/_ext/1909107292/mbrtu.o ../src/modbus/rtu/mbrtu.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1019015877/portevent.o: ../src/port/portevent.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1019015877" 
	@${RM} ${OBJECTDIR}/_ext/1019015877/portevent.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019015877/portevent.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1019015877/portevent.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/1019015877/portevent.o.d" -o ${OBJECTDIR}/_ext/1019015877/portevent.o ../src/port/portevent.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1019015877/portserial.o: ../src/port/portserial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1019015877" 
	@${RM} ${OBJECTDIR}/_ext/1019015877/portserial.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019015877/portserial.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1019015877/portserial.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/1019015877/portserial.o.d" -o ${OBJECTDIR}/_ext/1019015877/portserial.o ../src/port/portserial.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1019015877/porttimer.o: ../src/port/porttimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1019015877" 
	@${RM} ${OBJECTDIR}/_ext/1019015877/porttimer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1019015877/porttimer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1019015877/porttimer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/1019015877/porttimer.o.d" -o ${OBJECTDIR}/_ext/1019015877/porttimer.o ../src/port/porttimer.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/demo.o: ../src/demo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/demo.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/demo.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/demo.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/1360937237/demo.o.d" -o ${OBJECTDIR}/_ext/1360937237/demo.o ../src/demo.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/108032392/mb.o: ../src/modbus/mb.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/108032392" 
	@${RM} ${OBJECTDIR}/_ext/108032392/mb.o.d 
	@${RM} ${OBJECTDIR}/_ext/108032392/mb.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/108032392/mb.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -MMD -MF "${OBJECTDIR}/_ext/108032392/mb.o.d" -o ${OBJECTDIR}/_ext/108032392/mb.o ../src/modbus/mb.c    -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD) 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/modbus.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mdebugger -D__MPLAB_DEBUGGER_PK3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/modbus.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)    -mreserve=boot@0x1FC02000:0x1FC02FEF -mreserve=boot@0x1FC02000:0x1FC024FF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,--gc-sections,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/modbus.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/modbus.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default=$(CND_CONF)    $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--gc-sections,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	${MP_CC_DIR}/xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/modbus.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
